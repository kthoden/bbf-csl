---
csl: bbf.csl
bibliography: bildungsgeschichtede.json
lang: de
title: Tests für den BBF-Zitationsstil
subtitle: Mit fast allen Einträgen von bildungsgeschichte.de
link-bibliography: true
nocite: |
    @*
---
