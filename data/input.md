---
csl: bbf.csl
bibliography: examples.json
lang: de
title: Tests für den BBF-Zitationsstil
link-bibliography: true
---

# Zitation im Text:

[(@bethge2021; @frister1972)] und [(@kurig2016)]


## Zitationen ohne Seitenzahlen
- [(@baumert2017)]
- [(@bendavid2001)]
- [(@bethge2021)]
- [(@frister1972)]
- [(@helsper2007)]
- [(@hoffmannocon2020)]
- [(@kurig2015)]
- [(@lohmann2001a)]
- [(@schriftwechsel1990)]
- [(@seifert2022)]
- [(@taendler2015)]
- [(@tei1990)]
- [(@verfahrensweise)]
- [(@kallmeyer1908)]
- [(@kallmeyer1934)]
- [(@kallmeyer1935)]

## Einige Zitationen mit Seitenzahlen
- [(@baumert2017, 42)]
- [(@bendavid2001, S. 23-42)]
- [(@bethge2021, S. 42f.)]
- [(@frister1972, S. 23/42)]
- [(@helsper2007, S. 42)]
