# CSL BBF

Eine Format für Zitate und Bibliographien

Vorschau auf https://pages.tba-hosting.de/bbf-informationsmanagement/csl-bbf/index.html

## Zotero-Repositorium
Der Weg zur Veröffentlichung im [Zotero-Repositorium](https://www.zotero.org/styles) ist auf https://github.com/citation-style-language/styles/blob/master/CONTRIBUTING.md beschrieben.


## Zum Testen
Die Formatierung kann lokal mit `pandoc` getestet werden (https://pandoc.org/). Auf Windows läßt es sich leicht in der Power Shell mit `winget install pandoc` installieren (keine Administrator:innenrechte nötig).

Aufruf wie folgt:

    pandoc --citeproc --resource-path=src --resource-path=data data/input.md -s -o bbfcitation.html


## Nützliche Links
- https://docs.citationstyles.org/en/stable/specification.html
- https://editor.citationstyles.org/styleInfo/?styleId=http%3A%2F%2Fwww.zotero.org%2Fstyles%2Fbeltz-padagogik
- https://editor.citationstyles.org/visualEditor/


## Stilistische Vorgaben
- Originaldatum wird in das Feld "Extra" geschrieben, mit folgender Syntax:

        original-date: 1827
- DOI wird in das Feld "Extra" geschrieben, mit folgender Syntax:

        DOI: 10.14361/9783839453742
- Namenspartikeln wie "De" oder "Van" müssen in Zotero schon in das richtige Namensfeld eingetragen werden, dann werden sie auch richtig exportiert:

         editor = {Hoffmann-Ocon, Andreas and De Vincenti, Andrea and Grube, Norbert},

		{
			"family": "De Vincenti",
			"given": "Andrea"
		},

# Sprachversionen
Dadurch, daß das Attribut `default-locale` im Wurzelelement entfernt wird, gibt es in Zotero das Auswahlmenü, in welcher Lokalisierung/Sprache die Daten exportiert werden sollen (sieh https://forums.zotero.org/discussion/comment/286867#Comment_286867)