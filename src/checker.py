#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-
__version__ = "0.1"
__date__ = "20220915"
__author__ = "thoden@dipf.de"
__doc__ = """Check bibliography"""

import argparse
import configparser
from pathlib import Path
import urllib.request
import logging
import os
import json
import sys
import subprocess

logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s - %(levelname)s - %(message)s')

BASE_DIR = Path( os.path.realpath(__file__) ).parent
SCRIPT_NAME = Path( __file__).stem


def write_json_file(json_object, filename):
    """Write JSON file to disk"""

    outfile = open(filename, "w")
    outfile.write(json_object)
    logging.debug("Wrote %s", filename)
    outfile.close()
# def write_json_file ends here


def write_input_markdown(bib_items, language):
    """Prepare markdown document"""

    header = f"---\ncsl: bbf.csl\nlang: {language}\n"
    header += "title: Check\nlink-bibliography: false\n---\n\n"

    body = ""

    for entry in bib_items["items"]:
        body += f"[@{entry['id']}]\n"

    return header + body
    # def write_input_markdown ends here


def pandoc(filename, dotextension):
    """From https://stackoverflow.com/questions/12649908/calling-pandoc-from-python-using-subprocess-popen"""

    options = ['pandoc', f'{filename}.md', '-o', f'{filename}{dotextension}']
    options += ['--citeproc', '--wrap=none', '-s', '--to=html']
    options += ['--resource-path=src', f'--bibliography={filename}.json']
    logging.debug(options)  # for debugging
    return subprocess.check_call(options)
# def pandoc ends here


def main():
    """The main bit"""

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", help="Config file.", default = BASE_DIR / "checker.cfg",)
    parser.add_argument("-f", "--format", help="Export format.", default = "csljson")

    args = parser.parse_args()

    configs = configparser.ConfigParser()
    try:
        configs.read(args.config)
    except e:
        logging.error("Config file missing. Exiting.")
        sys.exit()

    zotero_group = configs["zotero"]["group"]
    api_key = configs["zotero"]["apikey"]

    api_url = f"https://api.zotero.org/groups/{zotero_group}/items?format={args.format}"

    zotero_request = urllib.request.Request(api_url, headers = {"Zotero-API-Key": api_key})
    logging.debug("Opening %s", api_url)

    with urllib.request.urlopen(zotero_request) as zotero_answer:
        logging.debug("Got answer: %s", zotero_answer.code)
        zotero_json = zotero_answer.read()
        entries = json.loads(zotero_json)
        logging.debug("Got %s entries.", len(entries["items"]))
        json_object = json.dumps(entries["items"])
        write_json_file(json_object, "test/wumpi.json")

    md = write_input_markdown(entries, "de")

    with open("test/wumpi.md", "w") as thefile:
        thefile.write(md)

    pandoc("test/wumpi", ".html")

    prefix = 13086757
    hash = {
        "58VNGT9B" : "seifert2022",
        "7F74BV52" : "baumert2017",
        "F35GN48V" : "bethge2021",
        "KE3DMNWT" : "taendler2015",
        "AQDJ3QQE" : "kurig2015",
        "G8254IDH" : "helsper2007",
        "ECXKZ55R" : "kurig2016",
        "3Z64CFX8" : "lohmann2001a",
        "J3K6IJVW" : "hoffmannocon2020",
        "7JIJWJSH" : "schriftwechsel1990",
        "8Z8PT634" : "tei1990",
        "WPE934SX" : "frister1972",
        "PAATWKJF" : "bendavid2001",
        "9FRKKZUZ" : "verfahrensweise"
    }
# def main ends here

if __name__ == '__main__':
    main()
# finis
