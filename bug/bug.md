---
csl: ../src/bbf.csl
bibliography: bug.json
lang: de
title: Found a bug?
link-bibliography: false
---

# shortTitle
[@gesetzst]

[@verfahrensweisest]

# title-short
[@gesetzts]

[@verfahrensweisets]

# Bibliographie
