# Problem

https://gitlab.tba-hosting.de/bbf-informationsmanagement/csl-bbf/-/issues/10

Export aus der Zotero-GUI mit Einstellung CSL-JSON gibt beim Feld Kurztitel den Key `title-short`, den ich in der CSL-Datei auch abfragen kann.

Über das API https://api.zotero.org/groups/4779975/items/3Z64CFX8?format=csljson kriege ich in dem Fall `shortTitle`. Ein Bug im API?


Benutze ich eine alte Version vom API?
